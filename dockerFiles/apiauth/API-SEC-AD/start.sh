export JAVA_HOME=$JAVA_HOME
export JAVA_OPTS=$JAVA_OPTS_ENV
export flinkVariables="--loadConf n --sink mq --logPath /var/todo1_apps/API-SEC-AD/logs/${HOSTNAME//./_}/ --logFile Gl_file-.*___.* --queueUrl tcp://${ACTIVEMQ}:61616 --queueName AMQTD1OMNIOSP::AMQTD1OMNIOSP.LOCALQ.AUDITFLINKQUEUE01 --queueUser admin --queuePwd todo1dev --readingFrequency 30 --savepointPath file:///var/todo1_apps/flink/savepoints"
mkdir -m755 /var/todo1_apps/API-SEC-AD/logs/${HOSTNAME//./_}/
/opt/flink-1.10.0/bin/start-cluster.sh
/opt/flink-1.10.0/bin/flink run -p 3 -d -c com.todo1.psf.flink.readers.ReadGLLogStreaming /var/todo1_apps/flink/psf-log-uploader-flink-2020.Q1.1.jar ${flinkVariables}
/opt/jboss/jboss-eap-6.4/bin/standalone.sh -Durl_images=${URL_IMAGES} -Djboss.server.name=${HOSTNAME//./_} -Dt1db.url=${DBURL} -Dt1db.user=${DBUSER} -Dt1db.pwd=${DBPWD} -Dt1db.smartproxy=${SMARTPROXY} -Dt1db.apisec=${APISEC} -Dt1db.pwdmgmt=${PWDMGMT} -Djboss.server.base.dir=/opt/jboss/runtimes/API-SEC-AD/standalone/ -server-config=standalone.xml