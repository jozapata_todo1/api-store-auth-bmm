/*******************************************************************************
 * © TODO1 SERVICES, INC. ('TODO1') All rights reserved, 2000, 2017.
 * 
 * This work is protected by the United States of America copyright laws.
 * All information contained herein is and remains the property of 
 * TODO1 [and its suppliers, if any] .
 * Dissemination of this information or reproduction of this material 
 * is not permitted unless prior written consent is obtained
 * from TODO1 SERVICES, INC.
 ******************************************************************************/
/**
 * 
 */
package com.todo1.as.auth.digitalaccess.retailbmm.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.todo1.psf.apistore.dto.beans.security.rs.StatusRS;
import com.todo1.psf.apistore.mua.beans.status.InternalStatus;
import com.todo1.psf.apistore.mua.services.ISaveParamsService;
import com.todo1.psf.apistore.mua.web.BaseController;
import com.todo1.psf.apistore.mua.web.util.ControllerMapper;
import com.todo1.psf.apistore.mua.web.util.SwaggerConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Controller for .... [customize] 
 * @author user
 *
 */
@Api(tags = {"YourTag"}, description = "Servicio personalizado para [objetivo]")
@RestController
@Scope("prototype")
@RequestMapping("/t1-psf-apistore-mua/rest/v1.0")
public class YourCustomController extends BaseController {

	/***
	 * Se indica una interface de Producto, pero puede ser modificada a una interface customizada
	 */
	private ISaveParamsService yourService;
	
	@Value("${yourService.enabled:false}")
	private Boolean yourServiceEnabled;
	
	/**
	 * Published service for .... [customize]
	 * @param flowId the flowId to execute
	 * @param request the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @return StatusRS with the mapped result of the execution
	 */
	@ApiOperation(value = "Servicio para [customizar]", notes = "Servicio para [customizar]", response = StatusRS.class)
	@ApiResponses(value = {
			@ApiResponse(code=200, message=SwaggerConstants.OPERACION_EXITOSA, response=StatusRS.class),
			@ApiResponse(code=503, message=SwaggerConstants.ERROR_COMUNICACIONES, response=StatusRS.class, reference=SwaggerConstants.ERROR_COMUNICACIONES_CODE)
	})
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@RequestMapping(method = RequestMethod.GET, value = "{flowId}/security/your/url", produces = { MediaType.APPLICATION_JSON_VALUE })
    public StatusRS doYourAction(@PathVariable @ApiParam(name = SwaggerConstants.FLOW_ID_NAME, required=true, value = SwaggerConstants.FLOW_ID_DESCR) String flowId,
    		HttpServletRequest request, HttpServletResponse response) {

		if (!yourServiceEnabled.booleanValue()) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return getDisabledServiceError(request);
		}
		InternalStatus iStatus = null;
		try {
			if (yourService == null) {
				yourService = (ISaveParamsService)appContext.getBean("yourService");
			}
			iStatus = yourService.doSaveParams(getServiceInfoBean(flowId, request));
			int httpCode = iStatus.getStatusCode() != null ? iStatus.getStatusCode() : HttpServletResponse.SC_OK;
			response.setStatus(httpCode);
		} catch (Exception e) {
			int httpCode = getHttpErrorCode(e);
			iStatus = buildErrorResponse(e, request);
			response.setStatus(httpCode);
		}
		return ControllerMapper.toStatusRS(iStatus);

    }

	/**
	 * @return the yourService
	 */
	public ISaveParamsService getYourService() {
		return yourService;
	}

	/**
	 * @param yourService the yourService to set
	 */
	public void setYourService(ISaveParamsService yourService) {
		this.yourService = yourService;
	}

	/**
	 * @return the yourServiceEnabled
	 */
	public Boolean getYourServiceEnabled() {
		return yourServiceEnabled;
	}

	/**
	 * @param yourServiceEnabled the yourServiceEnabled to set
	 */
	public void setYourServiceEnabled(Boolean yourServiceEnabled) {
		this.yourServiceEnabled = yourServiceEnabled;
	}

}


