/**
 * This package includes the Services interfaces. Implementations should be
 * under impl package. It can contain sub-packages for different categories.
 * 
 * com.todo1.[appl].[module].services
 * 
 * @author TODO1 Services
 *
 */
package com.todo1.as.auth.digitalaccess.retailbmm.custom.auth.services;