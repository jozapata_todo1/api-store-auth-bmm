/*******************************************************************************
 * © TODO1 SERVICES, INC. ('TODO1') All rights reserved, 2000, 2017.
 * 
 * This work is protected by the United States of America copyright laws.
 * All information contained herein is and remains the property of 
 * TODO1 [and its suppliers, if any] .
 * Dissemination of this information or reproduction of this material 
 * is not permitted unless prior written consent is obtained
 * from TODO1 SERVICES, INC.
 ******************************************************************************/
/**
 * 
 */
package com.todo1.as.auth.digitalaccess.retailbmm.services;

import java.util.List;

import com.todo1.psf.apistore.mua.svcwf.audit.IAuditor;
import com.todo1.tools.validation.beans.ValidationError;
import com.todo1.psf.apistore.mua.services.BaseService;

/**
 * @author mvisconti
 *
 */
public class CustomServiceImpl extends BaseService {

	public CustomServiceImpl(String eventCode, String stepName, String userBehaviour) {
		super(eventCode, stepName, userBehaviour);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object execute(Object... arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IAuditor getAuditor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ValidationError> validateInput(Object... arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}
