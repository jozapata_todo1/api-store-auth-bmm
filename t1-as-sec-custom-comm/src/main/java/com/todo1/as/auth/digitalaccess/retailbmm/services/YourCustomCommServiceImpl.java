/*******************************************************************************
 * © TODO1 SERVICES, INC. ('TODO1') All rights reserved, 2000, 2017.
 * 
 * This work is protected by the United States of America copyright laws.
 * All information contained herein is and remains the property of 
 * TODO1 [and its suppliers, if any] .
 * Dissemination of this information or reproduction of this material 
 * is not permitted unless prior written consent is obtained
 * from TODO1 SERVICES, INC.
 ******************************************************************************/
package com.todo1.as.auth.digitalaccess.retailbmm.services;

import java.util.ArrayList;
import java.util.List;

import com.todo1.as.auth.digitalaccess.comm.services.BaseCommunicationImpl;
import com.todo1.as.authcomm.base.beans.AuthenticationResponseBean;
import com.todo1.as.authcomm.base.beans.ContextInfoBean;
import com.todo1.as.authcomm.base.beans.UserCredentialsBean;
import com.todo1.as.authcomm.base.services.ICloseSessionCommService;
import com.todo1.tools.validation.beans.ValidationError;

/**
 * Service implementation. The ICloseSessionCommService interface is just an example, it can be also a custom interface
 *
 */
public class YourCustomCommServiceImpl extends BaseCommunicationImpl implements ICloseSessionCommService {

	@Override
	public AuthenticationResponseBean closeSession(ContextInfoBean context, UserCredentialsBean credentials) {
		return (AuthenticationResponseBean)doProcess(context, credentials);
	}

	@Override
	public String getUserBehaviour() {
		//customize your value, it will be used to audit
		return "USER_BEHAVIOUR_COMM";
	}

	@Override
	public Object execute(Object... params) {
		//your code here
		return null;
	}

	@Override
	public String getEventCode() {
		//customize your value, it will be used to audit
		return "EVENT_CODE_COMM";
	}

	@Override
	public List<ValidationError> validateInput(Object... arg0) {
		return new ArrayList<>();
	}

}
