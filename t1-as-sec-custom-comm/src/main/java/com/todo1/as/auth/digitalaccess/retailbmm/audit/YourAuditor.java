/*******************************************************************************
 * © TODO1 SERVICES, INC. All rights reserved, 2000 – 2016. This work is protected by the United States of America copyright laws. All information contained herein is and remains the property of TODO1 SERVICES. INC. Dissemination of this information or reproduction of this material is not permitted unless prior written consent is obtained from TODO1 SERVICES, INC.
 ******************************************************************************/
package com.todo1.as.auth.digitalaccess.retailbmm.audit;

import com.todo1.as.authsvcwf.audit.IAuditor;
import com.todo1.psf.logs.core.beans.GLTData;
import com.todo1.as.auth.digitalaccess.comm.audit.CommunicationAuditor;

/**
 * Auditor in comm layer
 *
 */
public class YourAuditor extends CommunicationAuditor implements IAuditor {

	@Override
	public void doLog(GLTData data) {
		super.doLog(data);
	}

	@Override
	public void fillRequest(GLTData data, Object[] params) {
		super.fillRequest(data, params);
	}

	@Override
	public void fillResponse(GLTData data, Object retorno, Exception exception) {
		super.fillResponse(data, retorno, exception);
	}

}
